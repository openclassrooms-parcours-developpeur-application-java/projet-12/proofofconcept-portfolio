package com.proofOfConceptportfolio.repository;

import com.proofOfConceptportfolio.model.ResourceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface ResourceRepository extends JpaRepository<ResourceEntity, Integer> {


    /**
     *  OBTENIR TOUTES LES RESSOURCES PAR L'ID DU PROOF OF CONCEPT
     * @param pocId
     * @return
     */
    @Query(value = "SELECT DISTINCT resource.* FROM resource" +
            " INNER JOIN resource_proofofconcept ON resource_proofofconcept.proof_of_concept_id= :pocId" +
            " WHERE resource.id= resource_proofofconcept.resource_id", nativeQuery = true)
    List<ResourceEntity> findAllResourceByProofOfConceptId(@Param("pocId") Integer pocId);


    /**
     * VERIFIE SI LE LIEN ENTRE LA TABLE RESOURCE ET PROOF OF CONCEPT EXISTE DEJA
     * @param resource_id
     * @param pocId
     * @return
     */
    @Query(value = "SELECT count(resource_proofofconcept) > 0 FROM resource_proofofconcept" +
            " WHERE resource_proofofconcept.resource_id= :resource_id AND resource_proofofconcept.proof_of_concept_id= :pocId", nativeQuery = true)
    Boolean resourceProofOfConcepttExists(@Param("resource_id") Integer resource_id, @Param("pocId") Integer pocId);


    /**
     * LIER DANS LA TABLE DE JOINTURE LA RESSOURCE AU PROOF OF CONCEPT
     * @param resourceId
     * @param pocId
     */
    @Modifying @Transactional
    @Query(value = "INSERT INTO resource_proofofconcept (resource_id, proof_of_concept_id)" +
            " VALUES (:resourceId, :pocId)", nativeQuery = true)
    int joinResourceToTheProofOfConcept(@Param("resourceId") Integer resourceId, @Param("pocId") Integer pocId);


    /**
     * SUPPRIMER LA JOINTURE ENTRE UNE RESSOURCE ET UN PROOF OF CONCEPT
     * @param resourceId
     * @param pocId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM resource_proofofconcept" +
            " WHERE resource_id= :resourceId AND proof_of_concept_id= :pocId", nativeQuery = true)
    int deleteResourceByProofOfConcept(@Param("resourceId") Integer resourceId, @Param("pocId") Integer pocId);


    /**
     * VERIFIE SI LE PROOF OF CONCEPT EXISTE
     * @param pocId
     * @return
     */
    @Query(value = "SELECT COUNT(proof_of_concept) > 0 FROM proof_of_concept WHERE proof_of_concept.id= :pocId", nativeQuery = true)
    Boolean proofOfConceptExists(@Param("pocId") Integer pocId);


    /**
     * OBTENIR LA RESSOURCE PAR SON URL
     * @param url
     * @return
     */
    ResourceEntity findByUrl(String url);

}
