package com.proofOfConceptportfolio.repository;

import com.proofOfConceptportfolio.model.ProofOfConceptEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ProofOfConceptRepository extends JpaRepository<ProofOfConceptEntity, Integer> {


    /**
     * SUPPRIMER UN PROOF OF CONCEPT PAR SON ID
     * @param pocId
     */
    @Modifying @Transactional
    @Query(value = "DELETE FROM proof_of_concept" +
            " WHERE proof_of_concept.id= :pocId", nativeQuery = true)
    int deleteProofOfConceptById(@Param("pocId") Integer pocId);


    /**
     * OBTENIR LE PROOF OF CONCEPT PAR SON NOM
     * @param proofOfConceptName
     * @return
     */
    ProofOfConceptEntity findByName(String proofOfConceptName);

}
