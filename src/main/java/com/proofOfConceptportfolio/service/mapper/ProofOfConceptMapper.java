package com.proofOfConceptportfolio.service.mapper;

import com.proofOfConceptportfolio.model.ProofOfConceptEntity;
import com.proofOfConceptportfolio.service.dto.ProofOfConceptDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ProofOfConceptMapper {

    ProofOfConceptMapper INSTANCE = Mappers.getMapper( ProofOfConceptMapper.class );

    ProofOfConceptDTO toDTO ( ProofOfConceptEntity proofOfConceptEntity );

    List<ProofOfConceptDTO> toDTOList ( List<ProofOfConceptEntity> proofOfConceptEntityList );

    ProofOfConceptEntity toEntity ( ProofOfConceptDTO proofOfConceptDTO );

    List<ProofOfConceptEntity> toEntityList ( List<ProofOfConceptDTO> proofOfConceptDTOList );
}
