package com.proofOfConceptportfolio.controller;

import com.proofOfConceptportfolio.exception.ProofOfConceptConflictException;
import com.proofOfConceptportfolio.exception.ProofOfConceptNotFoundException;
import com.proofOfConceptportfolio.model.ProofOfConceptEntity;
import com.proofOfConceptportfolio.model.ResourceEntity;
import com.proofOfConceptportfolio.proxy.MicroserviceMediaProxy;
import com.proofOfConceptportfolio.repository.ProofOfConceptRepository;
import com.proofOfConceptportfolio.repository.ResourceRepository;
import com.proofOfConceptportfolio.service.dto.ProofOfConceptDTO;
import com.proofOfConceptportfolio.service.dto.ResourceDTO;
import com.proofOfConceptportfolio.service.mapper.ProofOfConceptMapper;
import com.proofOfConceptportfolio.service.mapper.ResourceMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Api( description = "API POUR LES OPERATIONS CRUD / PROOF OF CONCEPT")
@RestController
public class ProofOfConceptController {

    private static final Logger logger = LoggerFactory.getLogger(ProofOfConceptController.class);

    @Autowired
    private ProofOfConceptRepository proofOfConceptRepository;

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private MicroserviceMediaProxy microserviceMediaProxy;


                                        /* ===================================== */
                                        /* ================ GET ================ */
                                        /* ===================================== */

    /* --------------------------------------- GET ALL PROOF OF CONCPET ---------------------------------------- */
    @ApiOperation( value = "GET ALL PROOF OF CONCEPT" )
    @GetMapping(value = "/poc/poc/get-all-poc")
    public List<ProofOfConceptDTO> getAllProofOfConcept() {

        List<ProofOfConceptDTO> proofOfConceptDTOList = new ArrayList<>();

        try {

            /**
             * JE RECUPERE TOUT LES PROOF OF CONCEPT
             * @see ProofOfConceptRepository
             */
            proofOfConceptDTOList = ProofOfConceptMapper.INSTANCE.toDTOList(proofOfConceptRepository.findAll());

            for (ProofOfConceptDTO proofOfConceptDTO : proofOfConceptDTOList) {
                /**
                 * JE RECUPERE LA LISTE DES IMAGES POUR CHAQUE PROOF OF CONCEPT
                 * @see MicroserviceMediaProxy#getListImageByProofOfConcept(Integer)
                 */
                proofOfConceptDTO.setImageDTOList(microserviceMediaProxy.getListImageByProofOfConcept(proofOfConceptDTO.getId()));

                /**
                 * JE RECUPERE LA LISTE DES RESSOURCES POUR CHAQUE PROOF OF CONCEPT
                 * @see ResourceRepository#findAllResourceByProofOfConceptId(Integer)
                 */
                proofOfConceptDTO.setResourceDTOList(ResourceMapper.INSTANCE.toDTOList(
                        resourceRepository.findAllResourceByProofOfConceptId(proofOfConceptDTO.getId())));
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return proofOfConceptDTOList;
    }


                                        /* ===================================== */
                                        /* ================ ADD ================ */
                                        /* ===================================== */

    /* ---------------------------------------- ADD PROOF OF CONCPET  --------------------------------------- */
    @ApiOperation( value = "ADD PROOF OF CONCEPT")
    @PostMapping(value = "/poc/poc/add-poc")
    public ProofOfConceptDTO addProofOfConcept(@RequestBody ProofOfConceptDTO proofOfConceptDTO ) {

        /** JE RECUPERE LA DATE COURANTE LORS DE LA CREATION D'UN NOUVEAU PROOF OF CONCEPT */
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        proofOfConceptDTO.setPublication(timestamp);

        ProofOfConceptEntity proofOfConceptEntity = proofOfConceptRepository.findByName(proofOfConceptDTO.getName());

        if (proofOfConceptEntity == null) {

            try {

                /** J'AJOUTE LE PROOF OF CONCEPT, PUIS JE RECUPERE SON IDENTIFIANT */
                proofOfConceptDTO.setId(ProofOfConceptMapper.INSTANCE.toDTO
                        (proofOfConceptRepository.save(ProofOfConceptMapper.INSTANCE.toEntity(proofOfConceptDTO))).getId());

            } catch (Exception pEX) {
                logger.error(String.valueOf(pEX));
            }

        } else {
            throw new ProofOfConceptConflictException("The proof of concept '" + proofOfConceptDTO.getName() +
                    "' already exists !");
        }
        return proofOfConceptDTO;
    }

    /* -------------------------------- ADD RESSOURCE FOR PROOF OF CONCPET  ---------------------------------- */
    @ApiOperation(value = "ADD RESOURCE FOR PROOF OF CONCEPT")
    @PostMapping(value = "/poc/resource/add-resource/{pocId}")
    public List<ResourceDTO> addResourceForPOC(@RequestBody List<ResourceDTO> resourceDTOList,
                                                  @PathVariable Integer pocId) {

        for (ResourceDTO resourceDTO : resourceDTOList) {

            /**
             * JE VERIFIE SI LA RESSOURCE EST DEJA PRESENTE DANS LA TABLE
             *  @see ResourceRepository#findByUrl(String)
             */
            ResourceEntity resourceEntity = resourceRepository.findByUrl(resourceDTO.getUrl());

            /**
             * SI LA RESSOURCE EST DEJA PRESENTE DANS LA TABLE, JE JOINS CELLE-CI A LA TABLE PROOF OF CONCEPT DANS
             * LA TABLE DE JOINTURE "RESOURCE_PROOFOFCONCEPT", SINON J'AJOUTE LA NOUVELLE RESSOURCE ET JE CREE LA JOINTURE
             */
            if (resourceEntity != null) {

                /**
                 * JE VERIFIE SI RESOURCE ET PROOF OF CONCEPT SON DEJA LIES
                 * @see ResourceRepository#resourceProofOfConcepttExists(Integer, Integer)
                 */
                Boolean resourceProofOfConcepttExists = resourceRepository.resourceProofOfConcepttExists(resourceEntity.getId(), pocId);

                if (!resourceProofOfConcepttExists) {
                    
                    try {

                        /** @see ResourceRepository#joinResourceToTheProofOfConcept(Integer, Integer) */
                        resourceRepository.joinResourceToTheProofOfConcept(resourceEntity.getId(), pocId);
                        resourceDTO.setId(resourceEntity.getId());
                        
                    } catch (DataIntegrityViolationException pEX) {
                        logger.error(String.valueOf(pEX));
                        throw new ProofOfConceptNotFoundException("Proof of concept not found for the ID : " + pocId +
                                " - Impossible to link the resource to the proof of concept");
                    } catch (Exception pEX) {
                        logger.error(String.valueOf(pEX));
                    }
                    
                } else {
                    throw new ProofOfConceptConflictException("The resource is already linked to the proof of concept : " + pocId);
                }

            } else {

                /**
                 * JE VERIFIE SI LE PROOF OF CONCEPT EXISTE
                 * @see ResourceRepository#proofOfConceptExists(Integer) 
                 */
                Boolean proofOfConceptExists = resourceRepository.proofOfConceptExists(pocId);
                
                if (proofOfConceptExists) {
                    
                    resourceDTO.setId(ResourceMapper.INSTANCE.toDTO(resourceRepository.save(
                            ResourceMapper.INSTANCE.toEntity(resourceDTO))).getId());

                    /** @see ResourceRepository#joinResourceToTheProofOfConcept(Integer, Integer) */
                    resourceRepository.joinResourceToTheProofOfConcept(resourceDTO.getId(), pocId);
                    
                } else {
                    throw new ProofOfConceptNotFoundException("Proof of concept not found for the ID : " + pocId +
                            " - Impossible to add the resource and link it to the proof of concept");
                }
            }
        }
        return resourceDTOList;
    }


                                        /* ===================================== */
                                        /* ============== UPDATE =============== */
                                        /* ===================================== */

    /* ---------------------------------------- UP PROOF OF CONCPET  ----------------------------------------- */
    @ApiOperation(value = "UP PROOF OF CONCEPT")
    @PutMapping(value = "/poc/poc/up-poc")
    public ProofOfConceptDTO upProofOfConcept(@RequestBody ProofOfConceptDTO proofOfConceptDTO) {

        /**
         * JE VERIFIE SI LE PROOF OF CONCPET EXISTE
         */
        Boolean proofOfConceptExists = proofOfConceptRepository.existsById(proofOfConceptDTO.getId());

        if (proofOfConceptExists) {

            try {
                proofOfConceptRepository.save(ProofOfConceptMapper.INSTANCE.toEntity(proofOfConceptDTO));
            } catch (Exception pEX) {
                logger.error("{}", String.valueOf(pEX));
            }
        } else {
            logger.error("Update failure - proof of concept : '" + proofOfConceptDTO.getName() + "' - Is not found");
            throw new ProofOfConceptNotFoundException("Update failure - proof of concept : '" + proofOfConceptDTO.getName() +
                    "' - Is not found");
        }
        return proofOfConceptDTO;
    }



                                        /* ======================================= */
                                        /* =============== DELETE  =============== */
                                        /* ======================================= */

    /* --------------------------------------- DEL PROOF OF CONCPET ------------------------------------------ */
    @ApiOperation( value = "DEL PROOF OF CONCPET COMPLETELY BY ID")
    @DeleteMapping( value = "/poc/poc/del-poc/{pocId}")
    public String delProofOfConcept(@PathVariable Integer pocId) {

        String massageStatus = "";

        try {

            Integer check = proofOfConceptRepository.deleteProofOfConceptById(pocId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }

    /* ------------------------------------ DEL RESOURCE BY PROOF OF CONCPE ------------------------------------ */
    @ApiOperation(value = "DEL RESOURCE BY PROOF OF CONCPE")
    @DeleteMapping(value = "/poc/resource/del-resource/{resourceId}/{pocId}")
    public String delResourceByProofOfConcept(@PathVariable Integer resourceId, @PathVariable Integer pocId) {

        String massageStatus = "";

        try {

            /** @see ResourceRepository#deleteResourceByProofOfConcept(Integer, Integer) */
            Integer check = resourceRepository.deleteResourceByProofOfConcept(resourceId, pocId);

            if (check == 0) {
                massageStatus = "Failed deletion - image not found";
            } else {
                massageStatus = "Successful deletion of the image";
            }

        } catch (Exception pEX) {
            logger.error(String.valueOf(pEX));
        }
        return massageStatus;
    }
}
