package com.proofOfConceptportfolio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ProofOfConceptConflictException extends RuntimeException {

    public ProofOfConceptConflictException(String s) {
        super(s);
    }
}
